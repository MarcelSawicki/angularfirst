Routing with modules

1. At first please type:
npm install

2. After that type this to start application.
npm start

Number Formatter
=========

A small library that adds commas to numbers

## Installation

  `npm install @msawicki/number-formatter`

## Usage

    var numFormatter = require('@msawicki/number-formatter');

    var formattedNum = numFormatter(35666);
  
  
  Output should be `35,666`


## Tests

  `npm test`

## Contributing

In lieu of a formal style guide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code.