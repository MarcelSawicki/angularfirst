import { Hero } from './Hero'

export const HEROES: Hero[] = [
  { id: 11, name: 'Mr. Nice', surname:'asd' },
  { id: 12, name: 'Narco', surname:'asd'  },
  { id: 13, name: 'Bombasto', surname:'asd'  },
  { id: 14, name: 'Celeritas', surname:'asd'  },
  { id: 15, name: 'Magneta', surname:'asd'  },
  { id: 16, name: 'RubberMan', surname:'asd'  },
  { id: 17, name: 'Dynama', surname:'asd'  },
  { id: 18, name: 'Dr IQ', surname:'asd'  },
  { id: 19, name: 'Magma', surname:'asd'  },
  { id: 20, name: 'Tornado', surname:'asd'  }
];