import { Otto } from './otto'

export var OTTE: Otto[] = [
    {id: 1, isSecret: false, name: 'Mr. Avr'},
    {id: 2, isSecret: true, name: 'Mr. Rnd'},
    {id: 3, isSecret: true, name: 'Mr. Cga'},
    {id: 4, isSecret: false, name: 'Mr. Vga'},
    {id: 5, isSecret: false, name: 'Mr. Ega'},
    {id: 6, isSecret: false, name: 'Mr. Cpc'}
];