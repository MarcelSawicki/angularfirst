import { Component } from '@angular/core';

import '../assets/css/styles.css';

@Component({
  selector: 'not-app',
  template: `<h1> Page not found! </h1>`
})
export class PageNotFoundComponent { }
